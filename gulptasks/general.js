import del   from 'del';
import gulp  from 'gulp';


// Собираем проект для разработки
gulp.task('build:dev', ['styles', 'scripts', 'pages', 'assets']);


// Команда по умолчанию. Выполняет сборку и запускает слежение за файлами
gulp.task('default', ['build:dev', 'server', 'watch']);


// Очистка директории от сборок
gulp.task('clean', () => { del(['./public/**', './build/**']) });
