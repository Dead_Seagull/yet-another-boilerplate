// Работа с шаблонами страниц

import gulp         from 'gulp';
import pug          from 'gulp-pug';
import plumber      from 'gulp-plumber';
import htmlpretty   from 'gulp-html-prettify';


// Переводим PUG в HTML
gulp.task('pages:transpile', () => {

  return gulp
    .src('./source/pages/*.pug')  	// Подаем на вход файлы страниц *.pug
    .pipe( plumber() )				// Запускаем отслеживание ошибок
    .pipe( pug({pretty: true}) )	// Транспайлим PUG в HTML
    .pipe( htmlpretty() )			// Наводим порядок в полученном HTML-коде
    .pipe( gulp.dest('./public/') ) // Сохраняем страницы в корне папки билда

});


// Main task
gulp.task('pages', ['pages:transpile']);
