// Отслеживание изменений

import gulp from 'gulp';


gulp.task('watch', () => {

  // Следим за стилями. При изменении запускаем общий таск для стилей - styles
  gulp.watch('./source/**/*.styl', ['styles']);

  // Следим за страницами. При изменении запускаем общий таск для страниц - pages
  gulp.watch('./source/pages/**/*.pug', ['pages']);

  // Следим за скриптами React. При изменении запускаем таск для вебпака
  gulp.watch('./source/app/**/*.{js.jsx}', ['scripts:webpack']);

  // Следим за общими скриптами проекта. При изменении запускаем таск для польз. скриптов
  gulp.watch(['./source/base/*.js', './source/blocks/**/*.js'], ['scripts:user']);

});
