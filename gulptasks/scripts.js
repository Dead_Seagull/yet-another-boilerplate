// Работа со скриптами

import gulp     from 'gulp';
import babel    from 'gulp-babel';
import concat   from 'gulp-concat';
import plumber  from 'gulp-plumber';
import webpack  from 'webpack-stream';
import srcmaps  from 'gulp-sourcemaps';


// Собираем React-приложение через webpack
gulp.task('scripts:webpack', () => {

	return gulp
		.src('./source/app/index.js') 						// Берем входной файл приложения
		.pipe( webpack(require('../webpack.config.js')) ) 	// Выполняем сборку зависимостей через webpack, передав файл конфига из корня проекта
		.pipe( gulp.dest('./public/js') ); 					// Сохраняем собранное приложение в папку со скриптами
		
});


// Обрабатываем пользовательские скрипты
gulp.task('scripts:user', () => {

	return gulp
		.src(['./source/base/*.js', './source/blocks/**/*.js']) // Берем пользовательские скрипты и скрипты блоков (см. BEM_)
		.pipe( plumber() )										// Запускаем отслеживание ошибок
		.pipe( babel({ presets: ["react", "es2015"] }))			// Прогоняем скрипты через Babel для перевод ES6 в валидный ES5
		.pipe( srcmaps.init() )									// Инициализируем сорсы
		.pipe( concat('scripts.js') )							// Склеиваем скрипты в единый файл	
		.pipe( srcmaps.write('./.maps') )						// Сохраняем сорсы в папку сборки
		.pipe( gulp.dest('./public/js') );						// Сохраняем результат в папку билда со скриптами

});


// Main task
gulp.task('scripts', ['scripts:webpack', 'scripts:user']); // Регистрируем таск с обработкой всех скриптов