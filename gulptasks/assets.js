// Работа с файлами

import gulp     from 'gulp';
import gulpif   from 'gulp-if';
import concat   from 'gulp-concat';
import plumber  from 'gulp-plumber';
import srcmaps  from 'gulp-sourcemaps';


// Указываем метод сборки - для разработки или продакшена
const dev = !process.env.NODE_ENV || process.env.NODE_ENV === 'dev';


// CSS LIBS
// Собираем .js и .css библиотеки в единые файлы
gulp.task('assets:libs', () => {

  return gulp
    .src('./source/assets/libs/*.css')      // Подаем на вход файлы css
    .pipe( plumber() )                      // Включаем отслеживание ошибок
    .pipe( srcmaps.init() )                 // Начинаем запись сорсов
    .pipe( concat('libs.css') )             // Склеиваем все библиотеки в один файл
    .pipe( srcmaps.write('./.maps') )       // Записываем сорсы в файл
    .pipe( gulp.dest('./public/css/') );    // Сохраняем результат в папку со стилями


  return gulp
    .src('./source/assets/libs/*.js')       // Подаем на вход файлы js
    .pipe( plumber() )                      // Включаем отслеживание ошибок
    .pipe( srcmaps.init() )                 // Начинаем запись сорсов
    .pipe( concat('libs.js') )              // Склеиваем все библиотеки в один файл
    .pipe( srcmaps.write('./.maps') )       // Записываем сорсы в файл
    .pipe( gulp.dest('./public/js/') );     // Сохраняем результат в папку со стилями

});


// ROOT
// Копируем рабочие файлы в папку билда без изменений
gulp.task('assets:static', () => {

  return gulp
    .src('./source/assets/static/**/*')     // Берем все папки и файлы с сохранением вложенности...
    .pipe( gulp.dest('./public') );         // ...и перемещаем в папку с билдом

});


// Main task
gulp.task('assets', ['assets:libs','assets:static']); // Регистрируем дефолтный таск для работы с файлами
