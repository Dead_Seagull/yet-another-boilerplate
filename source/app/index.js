import React    from 'react';
import ReactDOM from 'react-dom';
import App      from './App'

const appContainer = document.getElementById('mainApp');

ReactDOM.render(<App />, appContainer);