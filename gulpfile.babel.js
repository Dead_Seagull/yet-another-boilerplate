'use strict';

// Подключаем плагин require-dir
import requireDir  from 'require-dir';

// Подключаем к файлу все js-файлы с тасками из папки gulptasks
requireDir('./gulptasks');

// Постфикс 'babel' в названии файла нужен для корректной работы ES6