//  Подключаем вебпак
const webpack = require('webpack');
const path    = require('path');

module.exports = {

  // Контекст выполнения сборки - рабочая папка проекта
  context: path.join(__dirname, 'source'),

  // Включаем сорсмэпы
  devtool: "inline-sourcemap",

  // Подключаем лоадер для babel
  module: {
    loaders: [
      {
        test: /\.js?$/,                            // Выбираем файлы *.js
        exclude: /(node_modules|bower_components)/, // Исключаем из сборки папки 'node_modules' и 'bower_components'
        loader: 'babel-loader'                      // Указываем имя лоадера
      }
    ]
  },

  // Точка входа в приложение - файл .source/app/index.js
  entry: "./app/index.js",

  // Вывод сборки в папку со скриптами и с именем 'application.js'
  output: {
    path: path.join(__dirname, 'public', 'js'),
    filename: "application.js"
  }

};
